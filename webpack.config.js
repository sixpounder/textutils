const path = require('path');

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: './src/text-utils.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'text-utils.js'
  },
  node: {
    fs: 'empty'
  }
};
