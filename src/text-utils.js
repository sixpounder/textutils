const { parser } = require('./lexers/parser');

class TextUtils {
  strip (input) {
    return parser.parse(input);
  }
}

module.exports.TextUtils = TextUtils;
