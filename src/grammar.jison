%lex

%s ignore

%%

"/* hrz-restore-ignore-begin */"											      this.begin('ignore')
<ignore>"/* hrz-restore-ignore-end */"								      this.popState()
<ignore>[a-zA-Z0-9]+	                                    	/* skip */
<ignore>(.|\n)								                              /* skip */
<ignore><<EOF>>								                              /* skip */
[a-zA-Z0-9]+	                                    					return 'WORD'
(.|\n)								                                      return 'CHAR'
"/* hrz-restore-ignore */"											            return 'IGNORE_LINE'
<<EOF>>								                                      return 'EOF'

/lex

%start cleanable

%%

cleanable
  : contents EOF
    {return $1;}
  ;

contents
  : content
    {$$ = $1;}
  | contents content
    {$$ = $1 + $2;}
  ;
content
	: WORD
		{
			if (!yy.lexer.wordHandler) yy.lexer.wordHandler = function(word) {return word;};
			$$ = yy.lexer.wordHandler(yytext);
		}
	| CHAR
		{
			if (!yy.lexer.charHandler) yy.lexer.charHandler = function(char) {return char;};
			$$ = yy.lexer.charHandler(yytext);
		}
	| IGNORE_LINE content
		{
			$$ = '';
		}
 ;
