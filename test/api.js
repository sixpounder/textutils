/* eslint-disable no-undef */
require('mocha');
const assert = require('assert');
const { TextUtils } = require('../src/text-utils');
const fs = require('fs');

const parser = new TextUtils();
describe('Parser', function () {
  describe('Block strip', function () {
    it('should strip delimited block', function () {
      const template = parser.strip(fs.readFileSync(__dirname + '/templates/fn-block.js', 'utf-8'));
      const f = new Function(template);
      const result = f();
      assert.equal(result, 3);
    });

    it('should handle multiple blocks', function () {
      const template = parser.strip(fs.readFileSync(__dirname + '/templates/fn-multiple.js', 'utf-8'));
      const f = new Function(template);
      const result = f();
      assert.equal(result, 5);
    });

    it('should handle nested blocks', function () {
      const template = parser.strip(fs.readFileSync(__dirname + '/templates/fn-nested.js', 'utf-8'));
      const f = new Function(template);
      const result = f();
      assert.equal(result, 3);
    });

    it('should strip one-liners', function () {
      const template = parser.strip(fs.readFileSync(__dirname + '/templates/fn-oneliner.js', 'utf-8'));
      const f = new Function(template);
      const result = f();
      assert.equal(result, 'Hello World');
    });

    it('should strip mixed line markers', function () {
      const template = parser.strip(fs.readFileSync(__dirname + '/templates/fn-mixed.js', 'utf-8'));
      const f = new Function(template);
      const result = f();
      assert.equal(result, 'Hello World');
    });
  });
});
