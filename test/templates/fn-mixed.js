const getString = function () {
  return 'Hello World';
};

let s = getString();

/* hrz-restore-ignore-begin */s+= ' Gone Mad';
s += ' In Seconds';
/* hrz-restore-ignore-end */

return s;
